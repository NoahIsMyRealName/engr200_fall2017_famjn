Zombicide Round 1

Members: John, Noah Amy, Michael, Forest

Clarifications: There was not much that we disagreed with or felt that needed
to be added except for the addition of using the sewers as a means of escape. 
If a player found themselves in a situation where their lives were at stake, 
they could use the sewers in an attempt to find sanctuary from the hordes of 
mindless undead. 

Communication: 
    
    Before: Before the game had started our communication was more or less just
    trying to get to introduce ourselves to one another and go over some 
    questions about rules we may have not understood.
    
    During: During the game it was clear that we did not fully grasp all of the 
    rules and would constantly have to consult the rulebook over any 
    discrepancies we had while playing. As far as communication goes I would say
    we did a pretty good job, except there was a few times where it felt as 
    though we were talking over one another and I feel as though that is 
    something we will have to work on.
    
    After: After everything was said and done, we took a while to reflect on 
    how the round went and what we could improve on, the most obvious thing to
    work on was clarifying the rules so that there would be more time playing 
    and less time consulting the rulebook during each player's turn. Overall, I
    feel as though the next round will be more productive and will give us more 
    time to work communicating together as a team.